package com.task16_JSON.controller;

import com.task16_JSON.model.parser.GSONParser;
import com.task16_JSON.model.parser.JacksonParser;
import com.task16_JSON.model.validator.JsonValidator;
import java.io.FileNotFoundException;
import org.json.JSONException;

public class mainController {

  public static void main(String[] args) throws FileNotFoundException, JSONException {
    JacksonParser jacksonParser = new JacksonParser();
    jacksonParser.parse();
    GSONParser gsonParser = new GSONParser();
    try {
      gsonParser.parse();
    } catch (FileNotFoundException e) {
      System.out.println("File not found");
    }
//    JsonValidator validator = new JsonValidator();
//    System.out.println(validator
//        .validate("src\\main\\resources\\oldCards.json", "src\\main\\resources\\JSONSchema.json"));
  }
}
