package com.task16_JSON.model.validator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JsonValidator {

  public boolean validate(String filePath, String schemaPath) throws FileNotFoundException {
   try {
     JSONObject jsonSchema = new JSONObject(new JSONTokener(new FileReader(schemaPath)));
     JSONObject jsonSubject = new JSONObject(new JSONTokener(new FileReader(filePath)));
     Schema schema = SchemaLoader.load(jsonSchema);
     schema.validate(jsonSubject);
   } catch (ValidationException e) {
     System.out.println(e.getMessage());
     e.printStackTrace();
     return false;
   } catch (JSONException e) {
     e.printStackTrace();
   }
    return true;
  }

}
