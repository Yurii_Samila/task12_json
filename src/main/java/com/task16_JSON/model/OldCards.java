package com.task16_JSON.model;

import java.util.ArrayList;
import java.util.List;

public class OldCards {
  private List<Card> card = new ArrayList<>();

  public List<Card> getCard() {
    return card;
  }

  public void setCard(List<Card> card) {
    this.card = card;
  }

  @Override
  public String toString() {
    return "oldCards{" +
        "card=" + card +
        '}';
  }
}
