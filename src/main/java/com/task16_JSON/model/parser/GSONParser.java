package com.task16_JSON.model.parser;

import com.google.gson.Gson;
import com.task16_JSON.model.OldCards;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class GSONParser {

  public void parse() throws FileNotFoundException {
    Gson gson = new Gson();
    //File jSONFile = new FileReader("src\\main\\resources\\oldCards.json");
    OldCards oldCards = gson
        .fromJson(new FileReader("src\\main\\resources\\oldCards.json"), OldCards.class);
    System.out.println(oldCards);
  }
}
