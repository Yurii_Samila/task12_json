package com.task16_JSON.model.parser;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.task16_JSON.model.Card;
import com.task16_JSON.model.OldCards;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class JacksonParser {

  public void parse() {

    File jSONFile = new File("src\\main\\resources\\oldCards.json");
    ObjectMapper objectMapper = new ObjectMapper();
    OldCards oldCards;

    {
      try {
        oldCards = objectMapper.readValue(jSONFile, OldCards.class);
        System.out.println(oldCards);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
