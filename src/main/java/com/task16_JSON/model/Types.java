package com.task16_JSON.model;

import java.util.Objects;

public class Types {
  private Type type;
  private boolean sent;

  public Types(Type type, boolean sent) {
    this.type = type;
    this.sent = sent;
  }

  public Types() {
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public boolean isSent() {
    return sent;
  }

  public void setSent(boolean sent) {
    this.sent = sent;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Types type = (Types) o;
    return sent == type.sent &&
        this.type == type.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, sent);
  }

  @Override
  public String toString() {
    return "Type{" +
        "type=" + type +
        ", sent=" + sent +
        '}';
  }
}
