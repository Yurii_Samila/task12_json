package com.task16_JSON.model;

import java.util.Comparator;

public class CardComparator implements Comparator<Card> {

  @Override
  public int compare(Card o1, Card o2) {
    return o1.getYear()-o2.getYear();
  }
}
